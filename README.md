## Zombie Attack, version 0.3

This game is a free software. You may redistribute it under the terms of 
the GNU General Public License version 3 or later. See license.txt for 
details.    

## Content

1. General Information 
2. Installation
3. System requirements 
4. Contact Information
5. Authors

## 1.- General Information

This game was developed with the purpose to take advantage of the Unreal Engine and the oriented object programming paradigm.  
This project is using git in order to have versions and control of the project. 
The repository was given by The University of Nottingham.

#### Included in this release: 

- Virus modifier
- Outbreak
- BaseZone 
- Continent
- Country
- World
- Virus

## 2.- Installation

- Open ZombieUnrealGame/ZombieUnrealGame.sln
- Change Project settings to Release, x64
- Build project
- When finished, open ZombieUnrealGame/ZombieUnrealGame.uproject by Unreal 4.8.
- A prompt will show up, asking for rebuilding the Unreal project. Click OK and wait.
- After it finished rebuilding, the project is now recreated successfully and ready to be used.

## 3.- System requirements

#### Hardware Recommended:

- Operating System : Windows 7/8 64-bit
- Processor : Quad-core Intel or AMD, 2.5 GHz or faster
- Memory : 8 GB RAM
- Video Card/DirectX Version : DirectX 11 compatible graphics card

#### For developing:

- Unreal Engine Version:  Unreal Engine 4.8
- Visual Studio Version : Visual Studio 2013 Professional or Visual Studio 2013 Community:

## 4.- Contact Information

- Team : E
- Module : G54SWE
- Entity : University of Nottingham
- Address : Jubilee Campus, Wollaton Rd, Nottingham NG8 1BB

## 5.- Authors

- Anna Dodson
- Chris Brittian
- Edgar Vasquez
- Tung Vu
- Will Pimblett
