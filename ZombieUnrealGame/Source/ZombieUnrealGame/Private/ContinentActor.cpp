// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "ContinentActor.h"


// Sets default values
AContinentActor::AContinentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	continent = new CContinent(TCHAR_TO_UTF8(*name));
}

// Called when the game starts or when spawned
void AContinentActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AContinentActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	
	//loop through the array
	//World -> AddContinent( cActor[i]->GetContinentPointer() );
	//

}

bool AContinentActor::addCountry(ACountryActor* country){
	continent->AddCountry(country->getCountryPointer());
	
	return true;
}

int32 AContinentActor::getContinentHealthy(){
	return continent->getHealthy();
}

int32 AContinentActor::getContinentInfected(){
	return continent->getInfected();
}

int32 AContinentActor::getContinentDead(){
	return continent->getDead();
}

//int32 AWorldActor::getTotalLiving(){
//	return world->getTotalLiving();
//}

CContinent* AContinentActor::getContinentPointer(){
	return continent;
}

