// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "WorldActor.h"


// Sets default values
AWorldActor::AWorldActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	world = new CWorld();
}

// Called when the game starts or when spawned
void AWorldActor::BeginPlay()
{
	Super::BeginPlay();
	//loop through the array
	//World -> AddContinent( cActor[i]->GetContinentPointer() );
	//
}

bool AWorldActor::addContinent(AContinentActor* continent){
	world->AddContinent(continent->getContinentPointer());

	return true;
}

// Called every frame
void AWorldActor::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

	if (startCheck){
		count++;
		if (count > 30) {
			count = 0;
			m_iDayCount++;
			m_iDay++;
		}
	}
}

bool AWorldActor::unlockModifiers(){
	if (m_iDayCount >= 7){
		return true;
	}
	else
	{
		return false;
	}
}

int32 AWorldActor::getWorldHealthy(){
	return world->getHealthy();
}

int32 AWorldActor::getWorldInfected(){
	return world->getInfected();
}

int32 AWorldActor::getWorldDead(){
	return world->getDead();
}


CWorld* AWorldActor::getWorldPointer(){
	return world;
}