// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "MyDisplayActor.h"


// Sets default values
AMyDisplayActor::AMyDisplayActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyDisplayActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDisplayActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

