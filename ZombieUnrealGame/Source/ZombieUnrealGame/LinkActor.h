// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CountryActor.h"
#include "LinkActor.generated.h"

UCLASS()
class ZOMBIEUNREALGAME_API ALinkActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALinkActor();

	UPROPERTY(EditAnywhere)
		UShapeComponent* baseShape;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* theActorMesh;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		ACountryActor* country1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		ACountryActor* country2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FString type;
	
	bool init = false;
};
