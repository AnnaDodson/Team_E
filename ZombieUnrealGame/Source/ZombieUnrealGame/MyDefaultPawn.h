// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DefaultPawn.h"
#include "Country.h"
#include "CountryActor.h"
#include "VirusActor.h"
#include "MyDefaultPawn.generated.h"

class ACountryActor;

/**
 * 
 */
UCLASS()
class ZOMBIEUNREALGAME_API AMyDefaultPawn : public ADefaultPawn
{
	GENERATED_BODY()
	
private:

public:
	AMyDefaultPawn();

	//virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		bool firstVirus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		AVirusActor* virusActor;
	
	
	
};
