// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "MyHUD.h"
//#include "Test/Includes/MathFuncsLib.h"
#include "BaseVirus.h"

int AMyHUD::GetVirus(){
	CBaseVirus cbv(7, 2, 3, 4);
	return cbv.GetEffect();
	//return MathFuncs::MyMathFuncs::Add(1, 2);
}

AMyHUD::AMyHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}


void AMyHUD::DrawHUD()
{
	UE_LOG(LogTemp, Warning, TEXT("DrawHUD called"));
	FVector2D ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);

	Super::DrawHUD();

	//ABatteryGameCharacter* MyCharacter = Cast<ABatteryGameCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	int32 MyInt = GetVirus();
	FString IntAsString = FString::FromInt(MyInt);

	FString PowerLevelString = FString::Printf(TEXT("%10.1"), FMath::Abs(10));//MyCharacter->PowerLevel));

	//DrawText(IntAsString, FColor::White, 50, 50, GEngine->GetLargeFont());

	//ABatteryGameGameMode* MyGameMode = Cast<ABatteryGameGameMode>(UGameplayStatics::GetGameMode(this));

	//     if (MyGameMode->GetCurrentState() == EBatteryGamePlayState::EGameOver)
	//     {
	//         FVector2D GameOverSize;
	//         GetTextSize(TEXT("GAME OVER"), GameOverSize.X, GameOverSize.Y, HUDFont);
	// 
	//         DrawText(TEXT("GAME OVER"), FColor::White, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f, HUDFont);
	//     }
}
