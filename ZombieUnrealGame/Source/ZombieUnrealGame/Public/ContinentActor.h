// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Continent.h"
#include "CountryActor.h"
#include "ContinentActor.generated.h"

UCLASS()
class ZOMBIEUNREALGAME_API AContinentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	CContinent *continent;
	AContinentActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 population = 1000;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		FString name = " ";

	UFUNCTION(BlueprintCallable, Category = "Adapt Functions")
		bool addCountry(ACountryActor* country);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getContinentHealthy();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getContinentInfected();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getContinentDead();

	CContinent* getContinentPointer();
};
