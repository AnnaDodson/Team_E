// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "World.h"
#include "ContinentActor.h"
#include "WorldActor.generated.h"

UCLASS()
class ZOMBIEUNREALGAME_API AWorldActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	CWorld *world;
	AWorldActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 m_iDayCount = 0;

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 m_iDay = 0;

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 population = 1000;

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		bool startCheck = false;

	//Button Counters to check if the modifiers can enabled
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 effectButtonCounter = 0;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 minTempButtonCounter = 0;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 maxTempButtonCounter = 0;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 deathButtonCounter = 0;

	UFUNCTION(BlueprintCallable, Category = "Adapt Functions")
		bool addContinent(AContinentActor* continent);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		bool unlockModifiers();

	// Getter functions
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getWorldHealthy();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getWorldInfected();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getWorldDead();

	int32 count =0;

	CWorld* getWorldPointer();
};
