// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "../CountryActor.h"
#include "../Public/WorldActor.h"
#include "MyUserWidget2.generated.h"

/**
 * 
 */
UCLASS()
class ZOMBIEUNREALGAME_API UMyUserWidget2 : public UUserWidget
{
	GENERATED_BODY()


public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		ACountryActor* country;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		AWorldActor* world;

	
};
