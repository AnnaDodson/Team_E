// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "CountryActor.h"


// Sets default values
ACountryActor::ACountryActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	baseShape = CreateDefaultSubobject<UBoxComponent>(TEXT("basicshape"));
	RootComponent = baseShape;
	theActorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("themesh"));
	theActorMesh->AttachTo(baseShape);

	
}

// Called when the game starts or when spawned
void ACountryActor::BeginPlay()
{
	Super::BeginPlay();
	country = new CCountry((int)population, TCHAR_TO_UTF8(*name), (int)temperature);

}

// Called every frame
void ACountryActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	count++;
	if (count > 30) {
		count = 0;
		country->eventTick();
	}

}

#if WITH_EDITOR
void ACountryActor::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//Get all of our components  
	TArray<UActorComponent*> OwnedComponents;
	GetComponents(OwnedComponents);

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	// We test using GET_MEMBER_NAME_CHECKED so that if someone changes the property name  
	// in the future this will fail to compile and we can update it.  
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ACountryActor, name)))
	{
		//FMultiComponentReregisterContext ReregisterContext(OwnedComponents);
		delete country;
		country = new CCountry((int)population, TCHAR_TO_UTF8(*name));

		/*
		for (UActorComponent* Comp : OwnedComponents)
		{


		if (UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>(Comp))
		{
		MeshComp->StaticMesh = MyMesh; // Update the component to the new mesh
		}

		}
		*/
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ACountryActor, population)))
	{
		delete country;
		country = new CCountry((int)population, TCHAR_TO_UTF8(*name));
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif



int32 ACountryActor::getHealthy(){
	return country->getHealthy();
}

int32 ACountryActor::getInfected(){
	return country->getInfected();
}

int32 ACountryActor::getDead(){
	return country->getDead();
}

int32 ACountryActor::getTotalLiving(){
	return country->getTotalLiving();
}

bool ACountryActor::startOutbreak(AVirusActor* virusActor){
	//country->setOutbreak(new COutbreak(virusActor->getVirusPointer()));
	country->setOutbreak(new COutbreak(virusActor->getVirusPointer()));
	return true;
}

CCountry* ACountryActor::getCountryPointer(){
	return country;
}