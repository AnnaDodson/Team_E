// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "VirusActor.h"


// Sets default values
AVirusActor::AVirusActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVirusActor::BeginPlay()
{
	Super::BeginPlay();
	virus = new CVirus(effect, minTemp, maxTemp, deathRate);

	// Add a single modifier as constant baseline
	CVirusModifier* baseVirusModifier = new CVirusModifier();
	virus->AddModifier(baseVirusModifier);
	
}

// Called every frame
void AVirusActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

CVirus* AVirusActor::getVirusPointer(){
	return virus;
}

bool AVirusActor::addModifier(float effect, float minTemp, float maxTemp, float deathRate){
	virus->AddModifier(new CVirusModifier(effect, minTemp, maxTemp, deathRate));
	return true;
}
