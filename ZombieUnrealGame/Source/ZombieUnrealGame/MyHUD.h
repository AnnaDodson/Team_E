// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

/**
*
*/
UCLASS()
class ZOMBIEUNREALGAME_API AMyHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMyHUD(const FObjectInitializer& ObjectInitializer);


	UPROPERTY()
		UFont* HUDFont;

	/*primary draw call for the HUD*/
	virtual void DrawHUD() override;

	int GetVirus();


};
