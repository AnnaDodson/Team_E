// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Virus.h"
#include "VirusActor.generated.h"

UCLASS()
class ZOMBIEUNREALGAME_API AVirusActor : public AActor
{
	GENERATED_BODY()
private:
	CVirus* virus;
public:	
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		float effect;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		float minTemp;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		float maxTemp;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		float deathRate;

	UFUNCTION(BlueprintCallable, Category = "Adapt Functions")
		bool addModifier(float mEffect, float mMinTemp, float mMaxTemp, float dDeathRate);

	// Sets default values for this actor's properties
	AVirusActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	CVirus* getVirusPointer();
	
	
};
