// Fill out your copyright notice in the Description page of Project Settings.

#include "ZombieUnrealGame.h"
#include "LinkActor.h"


// Sets default values
ALinkActor::ALinkActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	baseShape = CreateDefaultSubobject<UBoxComponent>(TEXT("basicshape"));
	//baseShape->bGenerateOverlapEvents = true;
	//baseShape->OnComponentBeginOverlap.AddDynamic(this, &AMyNewNPC::TriggerEnter);
	RootComponent = baseShape;
	theActorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("themesh"));
	theActorMesh->AttachTo(baseShape);
}

// Called when the game starts or when spawned
void ALinkActor::BeginPlay()
{
	Super::BeginPlay();
	//TCHAR* chars = *type;
	
}

// Called every frame
void ALinkActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (!init) {
		init = true;
		country1->getCountryPointer()->addBorder(country2->getCountryPointer(), type.GetCharArray()[0]);
		country2->getCountryPointer()->addBorder(country1->getCountryPointer(), type.GetCharArray()[0]);
	}
}

