// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Country.h"
#include "VirusActor.h"
#include "CountryActor.generated.h"


UCLASS()
class ZOMBIEUNREALGAME_API ACountryActor : public AActor
{
	GENERATED_BODY()

	
public:	
	// Sets default values for this actor's properties
	CCountry *country;
	ACountryActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	
	UPROPERTY(EditAnywhere)
		UShapeComponent* baseShape;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* theActorMesh;
	
	UPROPERTY(EditAnywhere, Category = gameplay, BlueprintReadWrite)
		float speed;
	UPROPERTY(EditAnywhere, Category = gameplay, BlueprintReadWrite)
		FVector ourScale = FVector(1.5f, 1.5f, 1.5f);

	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 population=1000;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		int32 temperature = 20;
	UPROPERTY(EditAnywhere, Category = init, BlueprintReadWrite)
		FString name=" ";

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent) override;
#endif
	float runningTime;
	int32 count = 0;
	FVector playerStart = FVector(-750, 390, 226);


	// Getter functions
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getHealthy();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getInfected();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getDead();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Adapt Functions")
		int32 getTotalLiving();

	UFUNCTION(BlueprintCallable, Category = "Adapt Functions")
		bool startOutbreak(AVirusActor* virusActor);

	CCountry* getCountryPointer();
	
	
};
