// GroupEGameIntegration.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include "../GroupEGame/World.h"
#include "../GroupEGame/Continent.h"
#include "../GroupEGame/Country.h"
#include "../GroupEGame/Outbreak.h"
#include "../GroupEGame/Virus.h"
#include "../GroupEGame/VirusModifier.h"

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	cout << "---------- GroupEGame Integration Test ----------\n";

	CWorld* cWorld = new CWorld();
	CContinent* cContinentE = cWorld->AddContinent("Europe");
	CContinent* cContinentA = cWorld->AddContinent("Africa");
	CCountry* cCountry1 = cContinentE->AddCountry(40000, "UK", 18);
	CCountry* cCountry2 = cContinentA->AddCountry(50000, "Egypt", 25);

	cCountry1->addBorder(cCountry2, 'l');

	int pop1 = cCountry1->getHealthy();
	int pop2 = cCountry2->getHealthy();

	cout << "Population UK: H:" << cCountry1->getHealthy() <<
		" I: " << cCountry1->getInfected() <<
		" D: " << cCountry1->getDead() << "\n";

	cout << "Population Egypt: H:" << cCountry2->getHealthy() <<
		" I: " << cCountry2->getInfected() <<
		" D: " << cCountry2->getDead() << "\n";

	cout << "eventTick()\n";
	cCountry1->eventTick();
	cCountry2->eventTick();

	cout << "Population UK: H:" << cCountry1->getHealthy() <<
		" I: " << cCountry1->getInfected() <<
		" D: " << cCountry1->getDead() << "\n";
	cout << "Population Egypt: H:" << cCountry2->getHealthy() <<
		" I: " << cCountry2->getInfected() <<
		" D: " << cCountry2->getDead() << "\n";

	cout << "Add outbreak to UK\n";
	//create virus and add 1 modifier
	CVirus virus(0.005,17,23,0.00005);
	virus.AddModifier(new CVirusModifier(0,0,1,0));
	//add outbreak to country
	COutbreak* cOutbreak = new COutbreak(&virus);
	cCountry1->setOutbreak(cOutbreak);

	while (cCountry1->getHealthy() > 0 && cCountry2->getHealthy()>0) //|| cCountry1->getInfected() > 2 || cCountry1->getDead() > 2)
	{
		cout << "eventTick()\n";
		cCountry1->eventTick();
		cCountry2->eventTick();

		cout << "Population UK: H:" << cCountry1->getHealthy() <<
			" I: " << cCountry1->getInfected() <<
			" D: " << cCountry1->getDead() << "\n";
		cout << "Population Egypt: H:" << cCountry2->getHealthy() <<
			" I: " << cCountry2->getInfected() <<
			" D: " << cCountry2->getDead() << "\n";
	}

	int loss1 = (cCountry1->getInfected() + cCountry1->getDead());
	int loss2 = (cCountry2->getInfected() + cCountry2->getDead());

	cout << "---\n Finished, UK population loss: " << loss1 << "\n";
	cout << " Finished, Egypt population loss: " << loss2 << "\n";

	delete cWorld;

	return 0;
}

