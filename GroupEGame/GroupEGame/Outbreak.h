#pragma once
#include "vector"
#include "Country.h"
#include "Virus.h"
#include "VirusModifier.h"
#include "outbreakStruct.h"


#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

using namespace std;

class GROUPEGAME_API COutbreak :
	public CBaseGameObject
{
private:
	CVirus* m_pVirus;

public:
	COutbreak(CVirus* pVirus);
	~COutbreak();

	CVirus* GetVirusPointer();
	sOutbreak update(int healthy, int infected, int dead, float temperature);

};

