#pragma once
#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

#include "BaseVirus.h"
class GROUPEGAME_API CVirusModifier :
	public CBaseVirus
{
public:
	CVirusModifier();
	CVirusModifier(float effect, float minTemp, float maxTemp, float deathRate);
	~CVirusModifier();
};

