#pragma once
#include "stdafx.h"
#include "World.h"
#include "Continent.h"


CWorld::CWorld()
{
}


CWorld::~CWorld()
{
}

// Add an existing contient to the world
void CWorld::AddContinent(CContinent* continent)
{
	m_vpContinents.push_back(continent);
}

// Create a continent and add to the world, returns a ptr to new continent
CContinent* CWorld::AddContinent(string name)
{
	CContinent* c = new CContinent(name);
	m_vpContinents.push_back(c);
	return c;
}

vector<CContinent*> CWorld::GetContinents()
{
	return m_vpContinents;
}

// Compute number of healthy people
int CWorld::getHealthy()
{
	int ret = 0;
	for (CContinent* const& c : m_vpContinents) {
		ret += c->getHealthy();
	}
	return ret;
}

// Compute number of infected people
int CWorld::getInfected()
{
	int ret = 0;
	for (CContinent* const& c : m_vpContinents) {
		ret += c->getInfected();
	}
	return ret;
}

// Compute number of dead people
int CWorld::getDead()
{
	int ret = 0;
	for (CContinent* const& c : m_vpContinents) {
		ret += c->getDead();
	}
	return ret;
}
