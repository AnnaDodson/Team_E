#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

#pragma once
#include "BaseCharacter.h"

class GROUPEGAME_API CAIDirector :
	public CBaseCharacter
{
public:
	CAIDirector();
	~CAIDirector();
};

