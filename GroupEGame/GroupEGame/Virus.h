#pragma once
#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif
#include "vector"
#include "VirusModifier.h"
#include "BaseVirus.h"
class GROUPEGAME_API CVirus :
	public CBaseVirus
{
private:
	vector<CVirusModifier*> m_vpVirusModifiers;

	// Maximum levels the virus effect and death rate is allowed to reach.
	// Prevents the game running too quickly.
	const float MAX_EFFECT = 0.4;
	const float MAX_DEATH_RATE = 0.2;

public:

	CVirus();
	CVirus(float effect, float minTemp, float maxTemp, float deathRate);

	void AddModifier(CVirusModifier* virusModifier);

	float GetEffect(int healthy, int infected, int dead, float temperature);
    float GetDeathRate(int healthy, int infected, int dead, float temperature);
	float GetMinTemp();
	float GetMaxTemp();
	~CVirus();
};

