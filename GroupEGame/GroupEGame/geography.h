// Structs to store geographical relations between countries

#pragma once
#include <vector>
#include "Country.h"

using namespace std;

// Inform the compiler the class exists, without doing any def
class CCountry;

struct sBorder
{
	CCountry* pCountry;
	char cBorderMode; // "l" for land, "s" for sea

};

// Vector of borders
typedef vector<sBorder> vsBorderList;
