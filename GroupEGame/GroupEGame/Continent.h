#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

#pragma once
#include "BaseZone.h"
#include "Country.h"
#include <vector>
#include <string>

using namespace std;

class GROUPEGAME_API CContinent :
	public CBaseZone
{
public:
	CContinent(string name);
	~CContinent();

	vector <CCountry* > GetCountryList();
	CCountry* AddCountry(int population, string name, float temperature = 20);

	void AddCountry(CCountry* country);

	// Getter functions
	int getHealthy();
	int getInfected();
	int getDead();
	int getTotalLiving();

private:
	vector<CCountry*> m_vpCountries;
	string m_sContinentName;
};

