#pragma once
#include "BaseGameObject.h"
class CBaseMap :
	public CBaseGameObject
{
public:
	CBaseMap();
	~CBaseMap();

	int getHealthy();
	int getInfected();
	int getDead();
};

