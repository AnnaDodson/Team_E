#pragma once
#include "stdafx.h"
#include <string>
#include <vector>
#include "Continent.h"

using namespace std;

CContinent::CContinent(string name) :
m_sContinentName(name)
{
}


CContinent::~CContinent()
{
}

// Return a vector of all countries within the continent
vector<CCountry*> CContinent::GetCountryList()
{
	return m_vpCountries;
}

// Add existing country to continent
void CContinent::AddCountry(CCountry* country)
{
	m_vpCountries.push_back(country);
}

// Create a new country, add to continent and pass back a pointer
CCountry* CContinent::AddCountry(int population, string name, float temperature)
{
	CCountry* c = new CCountry(population, name, temperature);
	m_vpCountries.push_back(c);
	return c;
}

// Collect healthy population figure
int CContinent::getHealthy()
{
	int ret = 0;
	for (CCountry* const& c : m_vpCountries) {
		ret += c->getHealthy();
	}
	return ret;
}

// Collect infected population figure
int CContinent::getInfected()
{
	int ret = 0;
	for (CCountry* const& c : m_vpCountries) {
		ret += c->getInfected();
	}
	return ret;
}

// Collect dead population figure
int CContinent::getDead()
{
	int ret = 0;
	for (CCountry* const& c : m_vpCountries) {
		ret += c->getDead();
	}
	return ret;
}