#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

#pragma once
#include <vector>
#include <string>
#include "BaseMap.h"
#include "Continent.h"

using namespace std;

class GROUPEGAME_API CWorld :
	public CBaseMap
{
public:
	CWorld();
	~CWorld();

	void AddContinent(CContinent* continent);
	CContinent* AddContinent(string name);
	vector<CContinent*> GetContinents();

	// Getter functions
	int getHealthy();
	int getInfected();
	int getDead();
	int getTotalLiving();

private:
	vector<CContinent*> m_vpContinents; 

};

