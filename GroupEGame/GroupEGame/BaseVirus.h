#pragma once
#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif
#include "BaseGameObject.h"

using namespace std;

class GROUPEGAME_API CBaseVirus :
	public CBaseGameObject
{
protected:
	float m_fEffect;
	float m_fMinTemp;
	float m_fMaxTemp;
	float m_fDeathRate;
public:
	CBaseVirus(float effect, float minTemp, float maxTemp, float deathRate);
	float GetEffect();
	float GetMinTemp();
	float GetMaxTemp();
	float GetDeathRate();
	~CBaseVirus();
};

