// Structs to store geographical relations between countries

#pragma once
#include "Outbreak.h"

using namespace std;

// Inform the compiler the class exists, without doing any def
class COutbreak;

struct sOutbreak
{
	int iUpdateHealthy;
	int iUpdateInfected;
	int iUpdateDead;
};
