#pragma once
#include "stdafx.h"
#include <stdexcept>
#include <iostream>
#include <ctime>
#include "Country.h"
#include "geography.h"
#include "outbreakStruct.h"

CCountry::CCountry(int population, string name, float temperature) :
m_sCountryName (name)
{
	// Create a vsBorderList and store ref to it
	m_vsBorderList = new vsBorderList;
	
	// Set outbreak pointer to NULL to allow checking if set
	m_pOutbreak = NULL;

	// Set populations vars
	m_iHealthy = population;
	m_iInfected = 0;
	m_iDead = 0;

	m_iCrossingWait = 3;
	m_fPermeability = 0.5;

	// Ensure randomness is seeded
	//srand(time(NULL));

	m_fTemperature = temperature;
}


CCountry::~CCountry()
{
	// Delete *m_vsBorderList;
	delete m_vsBorderList;
	// If outbreak present, delete
	if (m_pOutbreak != NULL)
		delete m_pOutbreak;
}

// Add a border between this and another country, specifying 'l' for land and 's' for sea.
void CCountry::addBorder(CCountry* c, char m)
{
	sBorder newBorder;
	newBorder.pCountry = c;
	newBorder.cBorderMode = m;
	m_vsBorderList->push_back(newBorder);
}

// Return a ptr to the border list
vsBorderList CCountry::getBorderList()
{
	return *m_vsBorderList;
}

int CCountry::getHealthy()
{
	return m_iHealthy;
}

int CCountry::getInfected()
{
	return m_iInfected;
}

int CCountry::getDead()
{
	return m_iDead;
}

// Return the non-dead population
int CCountry::getTotalLiving()
{
	return m_iHealthy + m_iInfected;
}

// Assign an outbreak to the country
void CCountry::setOutbreak(COutbreak* pOutbreak)
{
	if (m_pOutbreak != NULL)
	{
		throw invalid_argument("Outbreak for this country has already been set");
	}
	else
	{
		// Assign outbreak and trigger start
		m_pOutbreak = pOutbreak;
		m_iInfected = 1;
		m_iDead = 1;
		m_iHealthy -= 2;
	}
}

void CCountry::eventTick()
{
	if (m_pOutbreak != NULL)
	{
		// Outbreak present, process population changes
		sOutbreak outbreakUpdate = m_pOutbreak->update(m_iHealthy, m_iInfected, m_iDead, m_fTemperature);
		// These need to be member variables of the returned struct
		m_iHealthy = outbreakUpdate.iUpdateHealthy;
		m_iInfected = outbreakUpdate.iUpdateInfected;
		m_iDead = outbreakUpdate.iUpdateDead;

		// Try crossing borders after a set number of wait ticks
		m_iCrossingWait--;
		if (m_iCrossingWait == 0) {
			CrossBorder();
		}
	}
	else
	{
		// No outbreak, do nothing
	}
}

void CCountry::SetPermeability(float permeability)
{
	m_fPermeability = permeability;
}

float CCountry::GetPermeability()
{
	return m_fPermeability;
}

void CCountry::CrossBorder()
{
	m_iCrossingWait = 3;
	//cross all borders
	for (int i = 0; i < m_vsBorderList->size(); i++) {
		sBorder currentBorder = m_vsBorderList->at(i);
		
		if (!currentBorder.pCountry->HasOutbreak()) {
			//percent of infected people will affect the crossing rate
			float infectedRate = getInfected() / (getInfected() + getHealthy());
			float permeability = currentBorder.pCountry->GetPermeability();
			//If there is a sea between 2 countries, harder to cross
			float seaPermeability = (currentBorder.cBorderMode) == 's' ? 0.5 : 1;
		
			float successRate = infectedRate * permeability * seaPermeability;
			//random chance
			//NOTE: Need to initialize srand() at main
			if (rand() % 100 <= successRate * 100) {
				currentBorder.pCountry->setOutbreak(new COutbreak(m_pOutbreak->GetVirusPointer()));
			}
		}
	}
	
}

bool CCountry::HasOutbreak()
{
	return m_pOutbreak != NULL;
}
