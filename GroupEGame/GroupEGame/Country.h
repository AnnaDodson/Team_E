#ifdef GroupEGame_EXPORTS
#define GROUPEGAME_API __declspec(dllexport)
#else
#define GROUPEGAME_API __declspec(dllimport)
#endif

#pragma once
#include <string>
#include "BaseZone.h"
#include "Outbreak.h"
#include "geography.h"


using namespace std;

// Inform compiler this class exists to break circular dependancy
class COutbreak;

class GROUPEGAME_API CCountry :
	public CBaseZone
{
public:
	
	CCountry(int population, string name, float temperature=20);
	~CCountry();

	vsBorderList getBorderList();

	void addBorder(CCountry*, char);

	// Getter functions
	int getHealthy();
	int getInfected();
	int getDead();
	int getTotalLiving();

	void setOutbreak(COutbreak* pOutbreak);

	void eventTick();

	//set and get permeability
	void SetPermeability(float permeability);
	float GetPermeability();
	//attempt to cross all borders
	void CrossBorder();
	bool HasOutbreak();

private:
	vsBorderList* m_vsBorderList;

	//How easy it is for outbreak from other countries to spread to this country
	float m_fPermeability;
	//How much ticks to wait before the next border crossing
	int m_iCrossingWait;

	float m_fTemperature; 

	int m_iHealthy;
	int m_iInfected;
	int m_iDead;
	string m_sCountryName;

	COutbreak* m_pOutbreak;
};

