#pragma once
#include "stdafx.h"
#include "VirusModifier.h"


CVirusModifier::CVirusModifier() :CBaseVirus(0.00005, 0, 0, 0.000001)
{
}

CVirusModifier::CVirusModifier(float effect, float minTemp, float maxTemp, float deathRate) : CBaseVirus(effect, minTemp, maxTemp, deathRate)
{
}


CVirusModifier::~CVirusModifier()
{
}
