#pragma once
#include "stdafx.h"
#include <ctime>
#include "Outbreak.h"
#include "outbreakStruct.h"


COutbreak::COutbreak(CVirus* pVirus){
	m_pVirus = pVirus;
	srand(std::time(NULL));
}


COutbreak::~COutbreak()
{
}


sOutbreak COutbreak::update(int healthy, int infected, int dead, float temperature) {
	float effect = m_pVirus->GetEffect(healthy, infected, dead, temperature);
	float deathRate = m_pVirus->GetDeathRate(healthy, infected, dead, temperature);

	sOutbreak updates;

	int iInfected = max(0, min(healthy * effect, healthy));
	int iKilled = max(0, min(infected * deathRate, infected));

	// Random chance kills and infections to seed outbreak
	if (iInfected == 0 && rand() % 100 <= 20)	{
		iInfected += max(0, min(rand() % 10, healthy));
	}
	// Freak infection events
	if (iInfected == 0 && rand() % 100 <= 3)	{
		iInfected += max(0, min(rand() % 500, healthy));
	}

	if (iKilled == 0 && rand() % 100 <= 5)	{
		//iKilled += max(0, min(1, infected));
	}


	int iNewHealthy = max(0, healthy - iInfected);
	int iNewInfected = max(0, infected + iInfected - iKilled);
	int iNewDead = max(0, dead + iKilled);

	updates.iUpdateHealthy = iNewHealthy;
	updates.iUpdateInfected = iNewInfected;
	updates.iUpdateDead = iNewDead;

	return updates;
}

CVirus* COutbreak::GetVirusPointer(){
	return m_pVirus;
}

