#pragma once
#include "stdafx.h"
#include "Virus.h"


CVirus::CVirus():CBaseVirus(0.01,20,30,0.008)
{
}

CVirus::CVirus(float effect, float minTemp, float maxTemp, float deathRate) : CBaseVirus(effect, minTemp, maxTemp, deathRate)
{
}


CVirus::~CVirus()
{
}

void CVirus::AddModifier(CVirusModifier* virusModifier){
	m_vpVirusModifiers.push_back(virusModifier);
}


float CVirus::GetEffect(int healthy, int infected, int dead, float temperature)
{
	float value = m_fEffect * ((float)infected /((float)healthy+1.0f));
	
	for (int i = 0; i < m_vpVirusModifiers.size(); i++) {
		value += m_vpVirusModifiers[i]->GetEffect();
	}

	// If temp out of range, reduce effect
	if (temperature > GetMaxTemp())
	{
		value = value * (1/(temperature - GetMaxTemp()));
	}
	else if (temperature < GetMinTemp())
	{
		value = value * (1/(GetMinTemp() - temperature));
	}


	// Prevent negative effect
	if (value < 0) return 0.0f;
	// Prevent huge effect
	if (value > MAX_EFFECT) return MAX_EFFECT;

	return value;
}

float CVirus::GetDeathRate(int healthy, int infected, int dead, float temperature)
{
	float value = m_fDeathRate * ((float)infected/ ((float)healthy + (float)infected + 1.0f));
	
	for (int i = 0; i < m_vpVirusModifiers.size(); i++) {
		value += m_vpVirusModifiers[i]->GetDeathRate();
	}

	// Prevent negative effect
	if (value < 0) return 0.0f;
	// Prevent huge effect
	if (value > MAX_DEATH_RATE) return MAX_DEATH_RATE;

	return value;
}

float CVirus::GetMinTemp()
{
	int value = m_fMinTemp;
	for (int i = 0; i < m_vpVirusModifiers.size(); i++) {
		value += m_vpVirusModifiers[i]->GetMinTemp();
	}
	return value;
}

float CVirus::GetMaxTemp()
{
	int value = m_fMaxTemp;
	for (int i = 0; i < m_vpVirusModifiers.size(); i++) {
		value += m_vpVirusModifiers[i]->GetMaxTemp();
	}

	return value;
}
