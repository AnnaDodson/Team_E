#pragma once
#include "stdafx.h"
#include "BaseVirus.h"
#include <stdexcept>

CBaseVirus::CBaseVirus(float effect, float minTemp, float maxTemp, float deathRate) : m_fEffect(effect), m_fMinTemp(minTemp), m_fMaxTemp(maxTemp), m_fDeathRate(deathRate)
{
	//max temperature must be larger or equal
	if (m_fMaxTemp < m_fMinTemp)
		throw invalid_argument("Minimum Temperature must be lesser than Maximum Temperature");
}


CBaseVirus::~CBaseVirus()
{
}

float CBaseVirus::GetEffect()
{
	return m_fEffect;
}

float CBaseVirus::GetMinTemp()
{
	return m_fMinTemp;
}

float CBaseVirus::GetMaxTemp()
{
	return m_fMaxTemp;
}

float CBaseVirus::GetDeathRate()
{
	return m_fDeathRate;
}