#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GroupEGame/World.h"
#include "../GroupEGame/Continent.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GroupEGameTest
{
	TEST_CLASS(WorldUnitTest)
	{
	public:
		
		TEST_METHOD(WorldClassInitialised)
		{
			CWorld world;
			Assert::IsNotNull(&world);
		}

		TEST_METHOD(CWorldAddContinents)
		{
			CWorld world;
			CContinent* continent = world.AddContinent("name");
			Assert::IsTrue(world.GetContinents().size() == 1);
		}

		TEST_METHOD(CWorldGetPopulationsTest)
		{
			CWorld world;
			CContinent* continent1 = world.AddContinent("name1");
			CContinent* continent2 = world.AddContinent("name2");
			const int CONST_POPULATIONS[3] = { 100, 200, 300 };
			continent1->AddCountry(CONST_POPULATIONS[0], "name 1");
			continent1->AddCountry(CONST_POPULATIONS[1], "name 2");
			continent2->AddCountry(CONST_POPULATIONS[2], "name 3");
			Assert::AreEqual(world.getHealthy(),
				CONST_POPULATIONS[0]
				+ CONST_POPULATIONS[1]
				+ CONST_POPULATIONS[2]
				);
		}

	};
}