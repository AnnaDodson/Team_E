#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GroupEGame/Country.h"
#include "../GroupEGame/Outbreak.h"
#include "../GroupEGame/geography.h"
#include <ctime>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace GroupEGameTest
{
	TEST_CLASS(CountryUnitTest)
	{
	public:
		
		TEST_METHOD(CCountryClassTest)
		{
			CCountry c(100, "name");
			Assert::IsNotNull(&c);
		}

		TEST_METHOD(CCountryBorderListTest)
		{
			CCountry countryA(100, "name");
			CCountry countryB(100, "name");
			countryA.addBorder(&countryB, 'l');
			vsBorderList testBorderList = countryA.getBorderList();
			Assert::IsTrue(testBorderList.size() == 1);
		}

		TEST_METHOD(CCountryPopulationTest)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			Assert::AreEqual(CONST_POPULATION, countryA.getHealthy());
		}

		TEST_METHOD(CCountryTotalLiving)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			Assert::AreEqual(CONST_POPULATION, countryA.getTotalLiving());
		}


		//###### PERMEABILITY TEST
		TEST_METHOD(CCountryGetPermeabilityReturnsAValue)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			countryA.SetPermeability(0.75);
			float value = 0.75;
			Assert::AreEqual(value, countryA.GetPermeability());
		}

		TEST_METHOD(CCountryGetPermeabilityReturnsCorrectValue)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			countryA.SetPermeability(0.34);
			float value = 0.34;
			Assert::AreEqual(value, countryA.GetPermeability());
		}

		TEST_METHOD(CCountryHasOutbreakReturnsCorrectValue)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			
			bool value = false;

			CVirus virus;
			virus.AddModifier(new CVirusModifier());

			countryA.setOutbreak(new COutbreak(&virus));
			value = true;
			Assert::IsTrue(countryA.HasOutbreak());
		}

		TEST_METHOD(CCountryCrossLandBorderSuccess)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			CCountry countryB(CONST_POPULATION, "name2");
			
			countryB.SetPermeability(0.1);

			countryA.addBorder(&countryB, 'l');

			CVirus virus;
			virus.AddModifier(new CVirusModifier());

			countryA.setOutbreak(new COutbreak(&virus));
			
			int count = 0;

			srand(time(NULL));

			for (int i = 0; i < 2000; i++)
			{
				count++;
				countryA.eventTick();
				if (countryB.HasOutbreak())
					break;
			}
			
			string s = to_string(count);
			Logger::WriteMessage("Land: ");
			Logger::WriteMessage(s.c_str());
			Assert::IsTrue(countryB.HasOutbreak());
		}

		TEST_METHOD(CCountryCrossSeaBorderSuccess)
		{
			const int CONST_POPULATION = 100;
			CCountry countryA(CONST_POPULATION, "name");
			CCountry countryB(CONST_POPULATION, "name2");

			countryB.SetPermeability(0.1);

			countryA.addBorder(&countryB, 's');

			CVirus virus;
			virus.AddModifier(new CVirusModifier());

			countryA.setOutbreak(new COutbreak(&virus));

			int count = 0;

			srand(time(NULL));

			for (int i = 0; i < 2000; i++)
			{
				count++;
				countryA.eventTick();
				if (countryB.HasOutbreak())
					break;
			}
			
			string s = to_string(count);
			Logger::WriteMessage("Sea: ");
			Logger::WriteMessage(s.c_str());
			Assert::IsTrue(countryB.HasOutbreak());
		}
	};
}