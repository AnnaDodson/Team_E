#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GroupEGame/BaseVirus.h"
#include "../GroupEGame/Virus.h"
#include "../GroupEGame/VirusModifier.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GroupEGameTest
{
	TEST_CLASS(VirusUnitTest)
	{
	public:
		
		TEST_METHOD(BaseVirusClassCreationTest)
		{
			CBaseVirus base(1,20,30,0.5);
			Assert::IsNotNull(&base);

		}

		TEST_METHOD(BaseVirusGetEffectReturnsCorrectValue)
		{
			CBaseVirus base(5, 20, 30, 0.5);
			float effect = base.GetEffect();
			float value = 5;
			Assert::AreEqual(effect, value);
		}

		TEST_METHOD(BaseVirusGetMinTempReturnsCorrectValue)
		{
			CBaseVirus base(1, 20, 30, 0.5);
			float minTemp = base.GetMinTemp();
			float value = 20;
			Assert::AreEqual(minTemp, value);
		}

		TEST_METHOD(BaseVirusGetMaxTempReturnsCorrectValue)
		{
			CBaseVirus base(1, 20, 40, 0.5);
			float maxTemp = base.GetMaxTemp();
			float value = 40;
			Assert::AreEqual(maxTemp, value);
		}

		TEST_METHOD(BaseVirusGetDeathRateReturnsCorrectValue)
		{
			CBaseVirus base(1, 20, 40, 0.75);
			float deathRate = base.GetDeathRate();
			float value = 0.75;
			Assert::AreEqual(deathRate, value);
		}

		TEST_METHOD(BaseVirusMinTempMustSmallerThanMaxTemp)
		{
			bool error = false;
			try {
				CBaseVirus base(1, 30, 20, 0.5);
			} catch (invalid_argument e) {
				error = true;
				char* value = "Minimum Temperature must be lesser than Maximum Temperature";
				Assert::AreEqual(e.what(), value);
			}
			if (!error)
				Assert::Fail();
			
		}





		TEST_METHOD(VirusClassCreationNoParameterTest)
		{
			// TODO: Your test code here
			CVirus virus;
			Assert::IsNotNull(&virus);

		}

		TEST_METHOD(VirusClassCreationWithParametersTest)
		{
			// TODO: Your test code here
			CVirus virus(1,2,3,4);
			Assert::IsNotNull(&virus);

		}

		TEST_METHOD(VirusModifierClassCreationNoParameterTest)
		{
			// TODO: Your test code here
			CVirusModifier modifier;
			Assert::IsNotNull(&modifier);

		}

		TEST_METHOD(VirusModifierClassCreationWithParameterTest)
		{
			// TODO: Your test code here
			CVirusModifier modifier(1,2,3,4);
			Assert::IsNotNull(&modifier);

		}
	};
}