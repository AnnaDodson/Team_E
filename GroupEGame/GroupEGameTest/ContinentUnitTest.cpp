#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GroupEGame/Country.h"
#include "../GroupEGame/Continent.h"
#include "../GroupEGame/World.h"
#include "../GroupEGame/Outbreak.h"
#include "../GroupEGame/geography.h"
#include <ctime>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace GroupEGameTest
{
	TEST_CLASS(ContinentUnitTest)
	{
	public:

		TEST_METHOD(CContinentClassTest)
		{
			CContinent c("name");
			Assert::IsNotNull(&c);
		}

		TEST_METHOD(CContinentGetPopulationsTest)
		{
			CContinent c("name");
			const int CONST_POPULATIONS[3] = { 100, 200, 300 };
			c.AddCountry(CONST_POPULATIONS[0], "name 1");
			c.AddCountry(CONST_POPULATIONS[1], "name 2");
			c.AddCountry(CONST_POPULATIONS[2], "name 3");
			Assert::AreEqual(c.getHealthy(),
				CONST_POPULATIONS[0]
				+ CONST_POPULATIONS[1]
				+ CONST_POPULATIONS[2]
			);
		}

	};
}