#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GroupEGame/Outbreak.h"
#include "../GroupEGame/Country.h"
#include "../GroupEGame/Virus.h"
#include "../GroupEGame/VirusModifier.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace GroupEGameTest
{
	TEST_CLASS(OutbreakUnitTest)
	{
	public:


		TEST_METHOD(OutbreakCreationTest)
		{
			CVirus virus;
			virus.AddModifier(new CVirusModifier());
			
			COutbreak outbreak(&virus);
			Assert::IsNotNull(&outbreak);
		}

		TEST_METHOD(OutbreakTest)
		{
			CVirus virus;
			virus.AddModifier(new CVirusModifier());

			COutbreak outbreak(&virus);
			sOutbreak update = outbreak.update(900, 100, 0, 20);
			Assert::IsNotNull(&update);
		}

	};
}